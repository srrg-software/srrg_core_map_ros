#pragma once

#include <srrg_ros_wrappers/ros_utils.h>
#include "map_msgs_ros.h"

namespace srrg_core_map_ros {

  class LocalMapListener {
  public:
    LocalMapListener(srrg_boss::IdContext* context_=0);

    void init(ros::NodeHandle& n);
    virtual void onNewLocalMap(srrg_core_map::LocalMap* lmap);
    virtual void onNewNode(srrg_core_map::MapNode* node);
    virtual void onNewRelation(srrg_core_map::BinaryNodeRelation* rel);
    virtual void onNewCameraInfo(srrg_core::BaseCameraInfo* cam);

  protected:
    void pinholeCameraInfoCallback(const srrg_core_ros::PinholeCameraInfoMsgConstPtr& msg);
    void multiCameraInfoCallback(const srrg_core_ros::MultiCameraInfoMsgConstPtr& msg);
    void imageMapNodeCallback(const srrg_core_ros::ImageMapNodeMsgConstPtr& msg);
    void multiImageMapNodeCallback(const srrg_core_ros::MultiImageMapNodeMsgConstPtr& msg);
    void relationsCallback(const srrg_core_ros::BinaryNodeRelationMsgConstPtr& msg);
    void localMapCallback(const srrg_core_ros::LocalMapMsgConstPtr& msg);
    
    void processPendingCameraInfos();
    void processPendingImageNodes();
    void processPendingRelations();
    void processPendingLocalMaps();
    void processPendingMsgs();
    
    std::list<srrg_core_ros::ImageMapNodeMsg> _pending_image_node_msgs;
    std::list<srrg_core_ros::MultiImageMapNodeMsg> _pending_multi_image_node_msgs;
    std::list<srrg_core_ros::PinholeCameraInfoMsg> _pending_camera_info_msgs;
    std::list<srrg_core_ros::MultiCameraInfoMsg> _pending_multi_camera_info_msgs;
    std::list<srrg_core_ros::LocalMapMsg> _pending_local_map_msgs;
    std::list<srrg_core_ros::BinaryNodeRelationMsg> _pending_relations_msgs;

    ros::Subscriber _sub_camera_info, _sub_multi_camera_info;
    ros::Subscriber _sub_image_map_node, _sub_multi_image_map_node;
    ros::Subscriber _sub_local_map;
    ros::Subscriber _sub_relations;
    std::set<srrg_boss::Identifiable*> _objects;
    srrg_boss::IdContext* _context;
  };

}
