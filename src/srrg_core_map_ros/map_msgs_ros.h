#include <srrg_ros_wrappers/ros_utils.h>
#include <srrg_boss/id_context.h>

#include <srrg_core_map/local_map.h>
#include <srrg_core_map/map_node.h>
#include <srrg_core_map/image_map_node.h>
#include <srrg_core_map/multi_image_map_node.h>
#include <srrg_core_map/binary_node_relation.h>
#include <srrg_types/base_camera_info.h>
#include <srrg_types/pinhole_camera_info.h>
#include <srrg_types/multi_camera_info.h>

#include <srrg_core_ros/RichPointMsg.h>
#include <srrg_core_ros/CloudMsg.h>
#include <srrg_core_ros/PinholeCameraInfoMsg.h>
#include <srrg_core_ros/MultiCameraInfoMsg.h>
#include <srrg_core_ros/MapNodeMsg.h>
#include <srrg_core_ros/ImageMapNodeMsg.h>
#include <srrg_core_ros/MultiImageMapNodeMsg.h>
#include <srrg_core_ros/LocalMapMsg.h>
#include <srrg_core_ros/BinaryNodeRelationMsg.h>

namespace srrg_core_map_ros {

  void msg2cloud(srrg_core::Cloud3D& dest,    const srrg_core_ros::CloudMsg& src);  
  void cloud2msg(srrg_core_ros::CloudMsg& dest,  const srrg_core::Cloud3D& src);

  srrg_core::PinholeCameraInfo* msg2pinholeCameraInfo(const srrg_core_ros::PinholeCameraInfoMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::PinholeCameraInfoMsg pinholeCameraInfo2msg(srrg_core::PinholeCameraInfo* src, srrg_boss::IdContext*  context);

  srrg_core::MultiCameraInfo* msg2multiCameraInfo(const srrg_core_ros::MultiCameraInfoMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::MultiCameraInfoMsg multiCameraInfo2msg(srrg_core::MultiCameraInfo* src, srrg_boss::IdContext*  context);

  srrg_core_map::MapNode* msg2MapNode(const srrg_core_ros::MapNodeMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::MapNodeMsg mapNode2msg(srrg_core_map::MapNode* src, srrg_boss::IdContext* context);

  srrg_core_map::ImageMapNode* msg2imageMapNode(const srrg_core_ros::ImageMapNodeMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::ImageMapNodeMsg imageMapNode2msg(srrg_core_map::ImageMapNode* src, srrg_boss::IdContext* context);

  srrg_core_map::MultiImageMapNode* msg2multiImageMapNode(const srrg_core_ros::MultiImageMapNodeMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::MultiImageMapNodeMsg multiImageMapNode2msg(srrg_core_map::MultiImageMapNode* src, srrg_boss::IdContext* context);

  srrg_core_map::LocalMap* msg2localMap(const srrg_core_ros::LocalMapMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::LocalMapMsg localMap2msg(srrg_core_map::LocalMap* src, srrg_boss::IdContext* context);
  
  srrg_core_map::BinaryNodeRelation* msg2binaryNodeRelation(const srrg_core_ros::BinaryNodeRelationMsg& msg, srrg_boss::IdContext* context);
  srrg_core_ros::BinaryNodeRelationMsg binaryNodeRelation2msg(srrg_core_map::BinaryNodeRelation* src, srrg_boss::IdContext* context);
  
}
