#include <srrg_system_utils/system_utils.h>
#include "srrg_core_map_ros/map_msgs_ros.h"
#include <ros/ros.h>
#include <srrg_messages/message_writer.h>
#include <tf/transform_listener.h>
#include <qapplication.h>
#include <srrg_gl_helpers/opengl_primitives.h>
#include "srrg_core_map_ros_viewers/global_map_viewer.h"

using namespace std;
using namespace srrg_boss;
using namespace srrg_core;
using namespace srrg_core_map_ros;
using namespace srrg_gl_helpers;

const char* banner[] = {
  "srrg_global_mapper_viewer_node: shows the global maps under construction",
  "usage:",
  " srrg_global_mapper_dumper_node ",
  "once the gui starts",
  "1: toggles/untoggles the current view (and saves a lot of bandwidth)",
  "shift + left click on a node: highlights the local map of the node",
  0
};
  
int main(int argc, char** argv) {
  if (argc>1){
    printBanner(banner);
    return 0;
  }

  ros::init(argc, argv, "srrg_global_mapper_viewer");
  ros::NodeHandle n;
  tf::TransformListener* listener = new tf::TransformListener(ros::Duration(60.0));

  QApplication* app=0; 
  GlobalMapViewer* viewer=0;
  
  app=new QApplication(argc, argv);
  viewer = new GlobalMapViewer();
  viewer->show();
  viewer->init(n, listener);
  

  while(ros::ok()){
    ros::spinOnce();
    app->processEvents();
    if (viewer->needRedraw()) {			
      viewer->updateGL();
    }
  }
}
