#include "srrg_core_map_ros/map_msgs_ros.h"
#include <ros/ros.h>
#include <srrg_messages/message_writer.h>
#include "srrg_core_map_ros/local_map_listener.h"

using namespace std;
using namespace srrg_boss;
using namespace srrg_core_map;
using namespace srrg_core_map_ros;
  
class LocalMapListenerExample : public LocalMapListener {
public:
  // override this and do what you want with the local map
  virtual void onNewLocalMap(LocalMap* lmap) {
    cerr << "got local map" << endl;
  }
};

int main(int argc, char** argv) {
  ros::init(argc, argv, "srrg_local_mapper_client_node");
  LocalMapListenerExample* example = new LocalMapListenerExample;
  ros::NodeHandle n;
  example->init(n);
  ros::spin();
}
