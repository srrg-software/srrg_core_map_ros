#include "srrg_core_map_ros/map_msgs_ros.h"
#include <srrg_core_ros/StampedCloudMsg.h>
#include <ros/ros.h>
#include <srrg_messages/message_writer.h>
#include "srrg_core_map_ros/local_map_listener.h"
#include <srrg_boss/serializer.h>
#include <srrg_system_utils/system_utils.h>

using namespace std;
using namespace srrg_boss;
using namespace srrg_core;
using namespace srrg_core_ros;
using namespace srrg_core_map;
using namespace srrg_core_map_ros;
  
class LocalMapDumper: public LocalMapListener {
public:
  LocalMapDumper(Serializer* ser) :
    LocalMapListener(ser){
    _ser= ser;
  }

  virtual void onNewLocalMap(LocalMap* lmap) {
    cerr << "got local map " << lmap->getId() <<endl;
    for ( MapNodeList::iterator it=lmap->nodes().begin(); 
	 it!=lmap->nodes().end(); it++){
      _ser->writeObject(*it->get());
    }
    for (BinaryNodeRelationSet::iterator it=lmap->relations().begin(); 
	 it!=lmap->relations().end(); it++){
      _ser->writeObject(*it->get());
    }
    _ser->writeObject(*lmap);
  }

  virtual void onNewNode(MapNode * n) {
    cerr << "got new node " << n->getId() <<endl;
    _ser->writeObject(*n);
  }

  virtual void onNewRelation(BinaryNodeRelation * r) {
    cerr << "got new relation " << r->getId() <<endl;
    _ser->writeObject(*r);
  }

  virtual void onNewCameraInfo(BaseCameraInfo * cam) {
    cerr << "got new camera info " << cam->getId() << endl;
    _ser->writeObject(*cam);
  }
  Serializer* _ser;
};

const char* banner[] = {
  "srrg_local_mapper_dumper_node: dumps incrementally the local maps made by a local mapper",
  "usage:",
  " srrg_local_mapper_dumper_node <filename>",
  0
};

int main(int argc, char** argv) {
  if (argc<2 || ! strcmp(argv[1],"-h")){
    printBanner(banner);
    return 0;
  }
  std::string output_filename = argv[1];
  ros::init(argc, argv, "srrg_local_mapper_dumper_node");
  Serializer * ser = new Serializer;
  ser->setFilePath(output_filename);
  ser->setBinaryPath(output_filename + ".d/<classname>.<nameAttribute>.<id>.<ext>");
  LocalMapDumper* dumper = new LocalMapDumper(ser);
  ros::NodeHandle n;
  dumper->init(n);
  ros::spin();
}
