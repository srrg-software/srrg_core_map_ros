#pragma once

#include "srrg_core_map_ros/map_msgs_ros.h"
#include <srrg_core_ros/StampedCloudMsg.h>
#include <ros/ros.h>
#include <srrg_messages/message_writer.h>
#include <tf/transform_listener.h>
#include <qapplication.h>
#include <srrg_core_map_viewers/trajectory_viewer.h>
#include "srrg_core_map_ros/local_map_listener.h"
#include <qevent.h>
#include "srrg_types/cloud_3d.h"

namespace srrg_core_map_ros {  

  class LocalMapViewer: public srrg_core_map_ros::LocalMapListener, public srrg_core_map_gui::TrajectoryViewer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    
    LocalMapViewer(srrg_boss::IdContext* context_=0);

    virtual void draw();
    virtual void onNewLocalMap(srrg_core_map::LocalMap* lmap);
    virtual void onNewNode(srrg_core_map::MapNode * n);
    virtual void onNewRelation(srrg_core_map::BinaryNodeRelation * r);
    void cloudCallback(const srrg_core_ros::StampedCloudMsgConstPtr& msg, Eigen::Isometry3f* pose, srrg_core::Cloud3D* dest);

    inline void setOriginFrameId(const std::string frame_id) {_origin_frame_id = frame_id;}
    inline const std::string& originFrameId() const {return _origin_frame_id;}
    
    void keyPressEvent(QKeyEvent *e);

    void setShowCurrentClouds(bool f);
    inline bool showCurrentClouds() const { return _show_clouds; }
    inline bool needRedraw() const {return _need_redraw;}
    void init(ros::NodeHandle& n, tf::TransformListener* tf_listener);

  protected:
    srrg_core::Cloud3D _reference;
    srrg_core::Cloud3D _current;
    bool _need_redraw;
    ros::NodeHandle * _n;
    std::string _origin_frame_id;
    ros::Subscriber _curr_sub, _ref_sub;
    Eigen::Isometry3f _curr_pose, _ref_pose;
    srrg_core::Cloud3D _curr_cloud, _ref_cloud;
    tf::TransformListener* _tf_listener;
    std::list<srrg_core_map::MapNode*> _temp_nodes; // nodes not yet in any of the local maps
    bool _show_clouds;
  };

}
